package microservice.mapper;

import microservice.core.model.Item;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ItemRowMapper implements RowMapper<Item> {

    @Override
    public Item mapRow(ResultSet resultSet, int i) throws SQLException {

        String key = resultSet.getString("item_key");
        String value = resultSet.getString("value");
        String alias = resultSet.getString("alias");
        boolean isActive = resultSet.getBoolean("is_active");
        String dictionaryName = resultSet.getString("dictionary_name");

        Item item;

        if (value != null) {
            item = Item.createKeyValueItem(dictionaryName, key, value);
        } else {
            item = Item.createKeyAliasItem(dictionaryName, key, alias);
        }

        if (isActive) {
            item.activateDictionary();
        } else {
            item.deactivateDictionary();
        }

        return item;
    }
}
