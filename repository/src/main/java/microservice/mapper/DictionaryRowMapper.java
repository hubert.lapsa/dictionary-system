package microservice.mapper;

import microservice.core.model.Dictionary;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DictionaryRowMapper implements RowMapper<Dictionary> {

    @Override
    public Dictionary mapRow(ResultSet resultSet, int i) throws SQLException {

        String dictionaryName = resultSet.getString("name");
        String subDictionaryName = resultSet.getString("sub_dictionary_name");
        boolean isActive = resultSet.getBoolean("is_active");

        Dictionary dictionary = new Dictionary(dictionaryName);

        if (subDictionaryName != null) {
            dictionary.setSubDictionaryName(subDictionaryName);
        }

        if (isActive) {
            dictionary.activateDictionary();
        } else {
            dictionary.deactivateDictionary();
        }

        return dictionary;
    }
}
