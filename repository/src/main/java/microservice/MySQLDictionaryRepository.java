package microservice;

import microservice.core.model.Dictionary;
import microservice.core.model.Item;
import microservice.core.repository.DirectoryRepository;
import microservice.mapper.DictionaryRowMapper;
import microservice.mapper.ItemRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository
public class MySQLDictionaryRepository implements DirectoryRepository {

    private final NamedParameterJdbcTemplate mySQLDataSource;

    private static final DictionaryRowMapper dictionaryRowMapper = new DictionaryRowMapper();
    private static final ItemRowMapper itemRowMapper = new ItemRowMapper();

    @Autowired
    public MySQLDictionaryRepository(NamedParameterJdbcTemplate mySQLDataSource) {
        this.mySQLDataSource = mySQLDataSource;
    }

    @Override
    public List<Dictionary> getAllDictionary() {

        return mySQLDataSource.query(
                "SELECT name, sub_dictionary_name, is_active FROM dict_conf",
                dictionaryRowMapper);
    }

    @Override
    public List<Item> getAllItemFromDictionary(Dictionary dictionary) {

        SqlParameterSource parameters = new MapSqlParameterSource("dictionaryName", dictionary.name());

        return mySQLDataSource.query(
                "SELECT item_key, value, alias, is_active, dictionary_name " +
                    "FROM dict_item " +
                    "WHERE dictionary_name = :dictionaryName", parameters, itemRowMapper);
    }

    @Override
    public void addDictionary(Dictionary dictionary) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name", dictionary.name());

        parameters.addValue("isActive", dictionary.isActive());

        if (dictionary.subDictionaryName() == null) {

            mySQLDataSource.update(
                    "INSERT INTO dict_conf (name, is_active) " +
                        "VALUES (:name, :isActive)",
                    parameters);

            return;
        }

        parameters.addValue("subDictionaryName", dictionary.subDictionaryName());

        mySQLDataSource.update(
                "INSERT INTO dict_conf (name, sub_dictionary_name, is_active) " +
                    "VALUES (:name, :subDictionaryName, :isActive)",
                parameters);
    }

    @Override
    public void changeDictionaryName(Dictionary dictionary, String newName) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name", dictionary.name());
        parameters.addValue("newName", newName);

        mySQLDataSource.update(
                "UPDATE dict_conf " +
                    "SET name = :newName " +
                    "WHERE name = :name",
                parameters
        );
    }

    @Override
    public void changeSubDictionary(Dictionary dictionary, String newSubDictionaryName) throws SQLException {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("name", dictionary.name());

        if (isDictionaryExist(parameters)) {
            parameters.addValue("newSubDictionary", newSubDictionaryName);

            mySQLDataSource.update(
                    "UPDATE dict_conf " +
                            "SET sub_dictionary_name = :newSubDictionary " +
                            "WHERE name = :name",
                    parameters
            );
        } else {
            throw new SQLException("Brak słownika");
        }
    }

    @Override
    public void activateDictionary(Dictionary dictionary) {

    }

    @Override
    public void deactivateDictionary(Dictionary dictionary) {

    }

    private boolean isDictionaryExist(MapSqlParameterSource parameters) {
        return mySQLDataSource.queryForList(
                "SELECT name " +
                        "FROM dict_conf " +
                        "WHERE name = :name",
                parameters).isEmpty();
    }
}
