package microservice.core.model.validator;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static microservice.core.model.validator.Validator.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidatorTest {

    @Test
    void shouldThrowExceptionWhenValueIsNull() {
        //then
        assertThrows(ValidationException.class, () -> {
            validateNullValue(null, "Value should not be null");
        }, "Validate null value");
    }

    @Test
    void shouldNotThrowExceptionWhenValueIsNull() {
        //then
        assertDoesNotThrow(() -> validateNullValue(new Object(), "Value should not be null"),
                "Validate not null value");

        //no exception
    }

    @Test
    void shouldThrowExceptionWhenValueIsEmpty() {
        //then
        assertThrows(ValidationException.class, () -> {
            validateNullAndEmptyValue("", "Value should not be empty");
        }, "Validate null value");
    }

    @Test
    void shouldThrowExceptionWhenValueisNull() {
        //then
        assertThrows(ValidationException.class, () -> {
            validateNullAndEmptyValue(null, "Value should not be empty");
        }, "Validate null value");
    }

    @Test
    void shouldNotThrowExceptionWhenValueIsNotEmpty() {
        //then
        assertDoesNotThrow(() -> validateNullAndEmptyValue("value", "Value should not be empty"),
                "Validate not null value");

        //no exception
    }

    @Test
    void shouldThrowExceptionWhenElementDoesNotExistInList() {
        //given
        List<String> list = asList("1", "2", "3");

        //then
        assertThrows(ValidationException.class, () -> {
            checkIsElementNotExistInList(list, "4", "Element already exist");
        }, "Validate is element exist in list");
    }

    @Test
    void shouldNotThrowExceptionWhenElementExistInList() {
        //given
        List<String> list = asList("1", "2", "3");

        //then
        assertDoesNotThrow(() -> checkIsElementNotExistInList(list, "3", "Element already exist"),
                "Validate is element exist in list");

        //no exception
    }

    @Test
    void shouldThrowExceptionWhenElementExistInList() {
        //given
        List<String> list = asList("1", "2", "3");

        //then
        assertThrows(ValidationException.class, () -> {
            checkIsElementExistInList(list, "3", "Element already exist");
        }, "Validate is element does not exist in list");
    }

    @Test
    void shouldNotThrowExceptionWhenElementNotExistInList() {
        //given
        List<String> list = asList("1", "2", "3");

        //then
        assertDoesNotThrow(() -> checkIsElementExistInList(list, "4", "Element already exist"),
                "Validate is element exist in list");

        //no exception
    }
}
