package microservice.core.repository;

import microservice.core.model.Dictionary;
import microservice.core.model.Item;

public interface ItemRepository {

    void addItemToDictionary(Dictionary dictionary, Item item);

    void updateItem(Dictionary dictionary, Item item);

    void activateItem(Dictionary dictionary, Item item);

    void deactivateItem(Dictionary dictionary, Item item);

    void deleteItem(Dictionary dictionary, Item item);
}
