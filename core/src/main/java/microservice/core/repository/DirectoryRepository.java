package microservice.core.repository;

import microservice.core.model.Dictionary;
import microservice.core.model.Item;

import java.sql.SQLException;
import java.util.List;

public interface DirectoryRepository {

    List<Dictionary> getAllDictionary();

    List<Item> getAllItemFromDictionary(Dictionary dictionary);

    void addDictionary(Dictionary dictionary);

    void changeDictionaryName(Dictionary dictionary, String newName);

    void changeSubDictionary(Dictionary dictionary, String newName) throws SQLException;

    void activateDictionary(Dictionary dictionary);

    void deactivateDictionary(Dictionary dictionary);
}
