package microservice.core.model;

import static microservice.core.model.validator.Validator.validateNullAndEmptyValue;
import static microservice.core.model.validator.Validator.validateNullValue;

public final class Item {

    private String key;
    private String value;
    private String alias;
    private boolean isActive = true;
    private String dictionaryName;

    private Item() {
    }

    public static Item createKeyValueItem(String dictionaryName, String key, String value) {
        validateNullAndEmptyValue(key, "Item key cannot be null or empty");
        validateNullAndEmptyValue(value, "Item value cannot be null or empty");
        validateNullAndEmptyValue(dictionaryName, "Dictionary name cannot be null or empty");

        Item item = new Item();
        item.key = key;
        item.value = value;
        item.dictionaryName = dictionaryName;

        return item;
    }

    public static Item createKeyAliasItem(String dictionaryName, String key, String alias) {
        validateNullAndEmptyValue(key, "Item key cannot be null or empty");
        validateNullAndEmptyValue(alias, "Item alias cannot be null or empty");
        validateNullAndEmptyValue(dictionaryName, "Dictionary name cannot be null or empty");

        Item item = new Item();
        item.key = key;
        item.alias = alias;
        item.dictionaryName = dictionaryName;

        return item;
    }

    public String key() {
        return key;
    }

    public void changeKey(String key) {
        this.key = key;
    }

    public String value() {
        return value;
    }

    public void changeValue(String value) {
        validateNullValue(this.value, "Item does not have value");
        validateNullAndEmptyValue(value, "New item value cannot be null or empty");

        this.value = value;
    }

    public String alias() {
        return alias;
    }

    public void changeAlias(String alias) {
        validateNullValue(this.value, "Item does not have alias");
        validateNullAndEmptyValue(value, "New alias cannot be null or empty");

        this.alias = alias;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void activateDictionary() {
        this.isActive = true;
    }

    public void deactivateDictionary() {
        this.isActive = false;
    }

    public String dictionaryName() {
        return this.dictionaryName;
    }
}
