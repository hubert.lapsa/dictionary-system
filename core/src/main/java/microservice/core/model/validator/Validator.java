package microservice.core.model.validator;

import java.util.Collection;

public final class Validator {

    private Validator() {}

    public static void validateNullValue(Object value, String message) {
        if (value == null) {
            throw new ValidationException(message);
        }
    }

    public static void validateNullAndEmptyValue(String value, String message) {
        if (value == null || value.isEmpty()) {
            throw new ValidationException(message);
        }
    }

    public static <T> void checkIsElementNotExistInList(Collection<T> list, T element, String message) {

        if (!list.contains(element)) {
            throw new ValidationException(message);
        }
    }

    public static <T> void checkIsElementExistInList(Collection<T> list, T element, String message) {

        if (list.contains(element)) {
            throw new ValidationException(message);
        }
    }
}
