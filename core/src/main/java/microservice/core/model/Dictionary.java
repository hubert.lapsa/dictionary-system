package microservice.core.model;

import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.unmodifiableList;
import static microservice.core.model.validator.Validator.*;

public class Dictionary {

    private String name;
    private String subDictionaryName;
    private final List<Item> items = new LinkedList<>();
    private boolean isActive = true;

    public Dictionary(String name) {
        validateNullAndEmptyValue(name, "Dictionary name cannot be empty");

        this.name = name;
    }

    public String name() {
        return this.name;
    }

    public void changeName(String name) {
        validateNullAndEmptyValue(name, "Dictionary name cannot be empty");

        this.name = name;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void activateDictionary() {
        this.isActive = true;
    }

    public void deactivateDictionary() {
        this.isActive = false;
    }

    public String subDictionaryName() {
        return this.subDictionaryName;
    }

    public void setSubDictionaryName(String subDictionaryName) {
        validateNullAndEmptyValue(name, "SubDictionary name cannot be empty");

        this.subDictionaryName = subDictionaryName;
    }

    public void addItem(Item item) {
        validateNullValue(item, "Item to add cannot be null");
        checkIsElementNotExistInList(items, item, "Element already exist in list of item");

        items.add(item);
    }

    public void removeItem(Item item) {
        validateNullValue(item, "Item to remove cannot be null");
        checkIsElementExistInList(items, item, "Element does not exist");

        items.remove(item);
    }

    public List<Item> items() {
        return unmodifiableList(this.items);
    }
}
