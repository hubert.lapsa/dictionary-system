package microservice.core.service;

import microservice.core.model.Dictionary;

import java.util.List;

public interface DictionaryService {

    List<Dictionary> getAllDictionary();

    boolean addDictionary(Dictionary dictionary);

    boolean updateDictionary(Dictionary dictionary);

    boolean activateDictionary(Dictionary dictionary);

    boolean deactivateDictionary(Dictionary dictionary);
}
