package microservice.core.service;

import microservice.core.model.Dictionary;
import microservice.core.model.Item;

import java.util.List;

public interface DictionaryItemService {

    List<Item> getAllItemFromDictionary(Dictionary dictionary);

    boolean addItemToDictionary(Dictionary dictionary, Item item);

    boolean updateItemInDictionary(Dictionary dictionary, Item item);

    boolean activateItemFromDictionary(Dictionary dictionary, Item item);

    boolean deactivateItemFromDictionary(Dictionary dictionary, Item item);

    boolean removeItemFromDictionary(Dictionary dictionary, Item item);

    //usługi dodatkowe
}
