package microservice.application.rest_exception_handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import static microservice.application.rest_exception_handler.ErrorMessage.notFoundMessage;
import static microservice.application.rest_exception_handler.ErrorMessage.serverErrorMessage;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ErrorMessage> handleNotFound() {
        return new ResponseEntity<>(notFoundMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleInternalServerError() {
        return new ResponseEntity<>(serverErrorMessage(), HttpStatus.NOT_FOUND);
    }
}
