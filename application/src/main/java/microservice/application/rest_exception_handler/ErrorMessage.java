package microservice.application.rest_exception_handler;

public final class ErrorMessage {

    private String message;

    private ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ErrorMessage notFoundMessage() {
        return new ErrorMessage("Resource not found");
    }

    public static ErrorMessage serverErrorMessage() {
        return new ErrorMessage("Internal application error");
    }
}
