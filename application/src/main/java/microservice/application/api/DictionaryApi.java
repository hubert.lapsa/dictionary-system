package microservice.application.api;

import microservice.application.dto.ItemApiDTO;
import microservice.core.model.Dictionary;
import microservice.core.repository.DirectoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DictionaryApi {

    private final DirectoryRepository mySQLDictionaryRepository;

    @Autowired
    public DictionaryApi(DirectoryRepository mySQLDictionaryRepository) {
        this.mySQLDictionaryRepository = mySQLDictionaryRepository;
    }

    @GetMapping(value = "allDictionary")
    public List<Dictionary> getAllDictionary() {
        List<Dictionary> allDictionary = mySQLDictionaryRepository.getAllDictionary();

        return allDictionary;
    }

    @GetMapping(value = "allDictionary1")
    public List<ItemApiDTO> getAllItems() {
        List<Dictionary> allDictionary = mySQLDictionaryRepository.getAllDictionary();

        //to service layer
        return mySQLDictionaryRepository.getAllItemFromDictionary(allDictionary.get(0)).stream()
                .map(ItemApiDTO::convertItemToDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "allDictionary21")
    public String getAllItems2() {
        Dictionary dictionary = new Dictionary("dict");

        mySQLDictionaryRepository.addDictionary(dictionary);

        return "aa";
    }

    @GetMapping(value = "update")
    public String update() throws SQLException {
        Dictionary dictionary = new Dictionary("bronek");
        mySQLDictionaryRepository.changeSubDictionary(dictionary, "dictionary");

        return "aa";
    }
}
