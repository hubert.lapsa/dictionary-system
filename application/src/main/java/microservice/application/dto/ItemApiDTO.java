package microservice.application.dto;

import microservice.core.model.Item;

public final class ItemApiDTO {

    private String key;
    private String value;
    private String alias;
    private boolean isActive;
    private String dictionaryName;

    private ItemApiDTO(String key, String value, String alias, boolean isActive, String dictionaryName) {
        this.key = key;
        this.value = value;
        this.alias = alias;
        this.isActive = isActive;
        this.dictionaryName = dictionaryName;
    }

    public static ItemApiDTO convertItemToDTO(Item item) {

        return new ItemApiDTO(item.key(), item.value(), item.alias(), item.isActive(), item.dictionaryName());
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getAlias() {
        return alias;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }
}
