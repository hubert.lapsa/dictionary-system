package microservice.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class MySQLDataSource {

    private static HikariConfig config = new HikariConfig();

    static {
        config.setJdbcUrl("jdbc:mysql://192.168.99.100:3306/dictionary_system");
        config.setUsername("hubert");
        config.setPassword("root");
    }

    private MySQLDataSource() {
    }

    public static HikariDataSource dataSource()  {
        return new HikariDataSource(config);
    }
}
