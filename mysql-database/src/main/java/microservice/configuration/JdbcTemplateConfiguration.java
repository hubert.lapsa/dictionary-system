package microservice.configuration;

import microservice.datasource.MySQLDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JdbcTemplateConfiguration {

    private static final DataSource MY_SQL_DATA_SOURCE = MySQLDataSource.dataSource();

    @Bean
    public DataSource dataSource() {
        return MY_SQL_DATA_SOURCE;
    }
}
